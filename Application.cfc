component displayname="Application" author="Nic Raboy" output="false" {

    this.name = "OAuth Demo";
    this.applicationTimeout = createTimeSpan(0, 0, 1, 0);
    this.sessionTimeout = createTimeSpan(0, 0, 60, 0);
    this.sessionManagement = true;

    variables.hasSSL = true; // True if SSL should be used

    variables.config = StructNew();
    variables.config["client_id"] = "CLIENT_ID_HERE";
    variables.config["client_secret"] = "CLIENT_SECRET_HERE";
    variables.config["redirect_uri"] = "REDIRECT_REGISTERED_WITH_GOOGLE_HERE";
    variables.config["approval_prompt"] = "force";
    variables.config["scope"] = ["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"];

    /*
     * Runs when ColdFusion receives the first request for a page in the application
     *
     * @param
     * @return   boolean
     */
    public boolean function onApplicationStart() output="false" {
        return true;
    }

    /*
     * Initiate / construct the OAuth session
     *
     * @param
     * @return
     */
    public void function onSessionStart() output="false" {
        session.oauth = createObject("component", "components/Google/OAuth/OAuth").init(variables.config);
        session.oauth.getRequestToken();
    }

    /*
     * Runs when a request starts
     *
     * @param
     * @return   boolean
     */
    public boolean function onRequestStart() output="false" {
        if(cgi.server_port != "443" && variables.hasSSL == true) {
            location("https://" & cgi.http_host & cgi.script_name, false);
        }
        if(!structKeyExists(session, "oauth") || !isInstanceOf(session.oauth, "components/Google/OAuth/OAuth")) {
            onSessionStart();
        }
        if(structKeyExists(url, "code")) {
            response = session.oauth.getAccessToken(url.code);
            session.accessToken = response.getAccessToken();
            session.refreshToken = response.getRefreshToken();
            location("/", false);
        }
        return true;
    }

    /*
     * Runs when a session ends
     *
     * @param    struct SessionScope
     * @param    struct ApplicationScope
     * @return
     */
    public void function onSessionEnd(struct SessionScope, struct ApplicationScope=structNew()) output="false" {
        if(structKeyExists(session, "oauth")) {
            structDelete(session, "oauth");
        }
        if(structKeyExists(session, "accessToken")) {
            structDelete(session, "accessToken");
            structDelete(session, "refreshToken");
        }
    }

    /*
     * Runs when a request specifies a non-existant CFML page
     *
     * @param    string TargetPage
     * @return   boolean
     */
    public boolean function onMissingTemplate(required string TargetPage) output="false" {
        switch(listLast(TargetPage, "/")) {
            case "logout.cfm":
                onSessionEnd();
                break;
        }
        return true;
    }

}