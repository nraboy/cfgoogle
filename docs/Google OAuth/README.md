Google OAuth 2.0
==============================

Google OAuth 2.0 components for ColdFusion
   

Full OAuth Configuration (Not usually necessary)
-------------

```
variables.config = StructNew();
variables.config["client_id"] = "CLIENT_ID_HERE";
variables.config["client_secret"] = "CLIENT_SECRET_HERE";
variables.config["redirect_uri"] = "https://www.example.com/callback";
variables.config["approval_prompt"] = "force|auto";
variables.config["response_type"] = "code";
variables.config["state"] = "";
variables.config["access_type"] = "online|offline";
variables.config["grant_type"] = "authorization_code|refresh_token";
variables.config["scope"] = ["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"];
```


Minimum OAuth Configuration
-------------

```
variables.config = StructNew();
variables.config["client_id"] = "CLIENT_ID_HERE";
variables.config["client_secret"] = "CLIENT_SECRET_HERE";
variables.config["redirect_uri"] = "https://www.example.com/callback";
variables.config["scope"] = ["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"];
```

Expected Results for OAuth::getAccessToken($requestToken)
-------------

OAuth::getAccessToken($requestToken) will return a Google\OAuth\Tokens\AccessToken object
with the following methods:

```
#!text
Google\OAuth\Tokens\AccessToken::getAccessToken();
Google\OAuth\Tokens\AccessToken::getRefreshToken();
Google\OAuth\Tokens\AccessToken::getTokenType();
Google\OAuth\Tokens\AccessToken::getExpirationTime();
```

OAuth::getAccessToken($requestToken) should only be called once during the session.


Example
-------------

    See Application.cfc for a full example


Resources
-------------

Google OAuth 2.0 Documentation - [https://developers.google.com/accounts/docs/OAuth2WebServer](https://developers.google.com/accounts/docs/OAuth2WebServer)