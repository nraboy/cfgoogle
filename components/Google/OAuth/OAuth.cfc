component displayname="OAuth" author="Nic Raboy" output="false" {

    property struct params;

    public oauth function init(struct params=StructNew()) output="false" {
        variables.params = arguments.params;
        return this;
    }

    public void function getRequestToken() output="false" {
        requestToken = createObject("component", "Requests/RequestToken").init(variables.params);
        location(requestToken.generateUri(), false);
    }

    public any function getAccessToken(required string requestToken) output="false" {
        accessToken = createObject("component", "Requests/AccessToken").init(arguments.requestToken, variables.params);
        response = accessToken.exchange();
        return createObject("component", "Tokens/AccessToken").init(deserializeJSON(response));
    }

}