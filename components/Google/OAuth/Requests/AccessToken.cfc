component displayname="AccessToken" author="Nic Raboy" accessors="true" output="false" {

    property string accessUri;
    property name="clientId" type="string" default="";
    property name="clientSecret" type="string" default="";
    property name="redirectUri" type="string" default="";
    property name="grantType" type="string" default="authorization_code";
    property name="requestToken" type="string" default="";

    public accesstoken function init(string requestToken, struct params=StructNew()) output="false" {
        variables.accessUri = "https://accounts.google.com/o/oauth2/token";
        setClientId(structKeyExists(arguments.params, "client_id") ? arguments.params["client_id"] : "");
        setClientSecret(structKeyExists(arguments.params, "client_secret") ? arguments.params["client_secret"] : "");
        setRedirectUri(structKeyExists(arguments.params, "redirect_uri") ? arguments.params["redirect_uri"] : "");
        setGrantType(structKeyExists(arguments.params, "grant_type") ? arguments.params["grant_type"] : "authorization_code");
        setRequestToken(arguments.requestToken);
        return this;
    }

    public string function exchange() output="false" {
        httpRequest = new http();
        httpRequest.setUrl(getAccessUri());
        httpRequest.setMethod("POST");
        httpRequest.addParam(type="formfield", name="client_id", value="#getClientId()#");
        httpRequest.addParam(type="formfield", name="client_secret", value="#getClientSecret()#");
        httpRequest.addParam(type="formfield", name="redirect_uri", value="#getRedirectUri()#");
        httpRequest.addParam(type="formfield", name="grant_type", value="#getGrantType()#");
        httpRequest.addParam(type="formfield", name="code", value="#getRequestToken()#");
        httpResult = httpRequest.send().getPrefix();
        return httpResult.fileContent;
    }

}