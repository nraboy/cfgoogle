/*
 * Requests/RequestToken
 *
 * Created by Nic Raboy
 *
 * The AccessToken response received from Google.  Originally JSON, but converted into a 
 * a structure
 *
 */
component displayname="RequestToken" author="Nic Raboy" accessors="true" output="false" {

    property name="requestUri" type="string" default="";
    property name="responseType" type="string" default="";
    property name="clientId" type="string" default="";
    property name="redirectUri" type="string" default="";
    property name="approvalPrompt" type="string" default="auto";
    property name="state" type="string" default="";
    property name="accessType" type="string" default="offline";
    property name="scope" type="array" default="";

    public requesttoken function init(struct params=StructNew()) output="false" {
        variables.requestUri = "https://accounts.google.com/o/oauth2/auth";
        setResponseType(structKeyExists(arguments.params, "response_type") ? arguments.params["response_type"] : "code");
        setClientId(structKeyExists(arguments.params, "client_id") ? arguments.params["client_id"] : "");
        setRedirectUri(structKeyExists(arguments.params, "redirect_uri") ? arguments.params["redirect_uri"] : "");
        setApprovalPrompt(structKeyExists(arguments.params, "approval_prompt") ? arguments.params["approval_prompt"] : "auto");
        setState(structKeyExists(arguments.params, "state") ? arguments.params["state"] : "");
        setAccessType(structKeyExists(arguments.params, "access_type") ? arguments.params["access_type"] : "offline");
        setScope(structKeyExists(arguments.params, "scope") ? arguments.params["scope"] : []);
        return this;
    }

    /*
     * Assemble only the GET parameters required for a request token
     *
     * @param
     * @return   string
     */
    public string function assembleParams() output="false" {
        requestStruct = StructNew();
        requestStruct["response_type"] = getResponseType();
        requestStruct["client_id"] = getClientId();
        requestStruct["redirect_uri"] = getRedirectUri();
        requestStruct["approval_prompt"] = getApprovalPrompt();
        requestStruct["state"] = getState();
        requestStruct["access_type"] = getAccessType();
        requestStruct["scope"] = arrayToList(getScope(), " ");
        assembled = "";
        for(key in requestStruct) {
            assembled = assembled & key & "=" & requestStruct[key] & "&";
        }
        return assembled;
    }

    /*
     * Generate the URL with parameters attached
     *
     * @param
     * @return   string
     */
    public string function generateUri() output="false" {
        return variables.requestUri & "?" & assembleParams();
    }

}