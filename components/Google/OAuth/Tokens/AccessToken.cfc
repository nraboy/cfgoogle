/*
 * Tokens/AccessToken
 *
 * Created by Nic Raboy
 *
 * The AccessToken response received from Google.  Originally JSON, but converted into a 
 * a structure
 *
 */
component displayname="AccessToken" author="Nic Raboy" accessors="true" output="false" {

    /*
     * Each element of the JSON object returned from Google
     */
    property name="accessToken" type="string" default="";
    property name="refreshToken" type="string" default="";
    property name="tokenType" type="string" default="Bearer";
    property name="expirationTime" type="string" default="";

    public accesstoken function init(struct params=StructNew()) output="false" {
        setAccessToken(structKeyExists(arguments.params, "access_token") ? arguments.params["access_token"] : "");
        setRefreshToken(structKeyExists(arguments.params, "refresh_token") ? arguments.params["refresh_token"] : "");
        setTokenType(structKeyExists(arguments.params, "token_type") ? arguments.params["token_type"] : "");
        setExpirationTime(structKeyExists(arguments.params, "expires_in") ? arguments.params["expires_in"] : "auto");
        return this;
    }

}